<?php

namespace Drupal\diff_preview\Routing;

use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\RouteCollection;

/**
 * Custom route subscriber to alter access and controller for diff preview.
 */
class RouteSubscriber extends RouteSubscriberBase {

  /**
   * {@inheritdoc}
   */
  protected function alterRoutes(RouteCollection $collection) {
    if ($route = $collection->get('diff.revisions_diff')) {
      $route->setRequirements([
        '_custom_access' => 'diff_preview.diff_access_check::access',
      ]);
      $route->setDefault('_controller', '\Drupal\diff_preview\Controller\PreviewDiffController::compareNodeRevisions');
    }
  }

}
