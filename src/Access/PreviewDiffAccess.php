<?php

namespace Drupal\diff_preview\Access;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Routing\Access\AccessInterface;
use Drupal\Core\Routing\RouteMatch;
use Drupal\Core\Session\AccountInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Custom AccessCheck for revision access and token access.
 */
class PreviewDiffAccess implements AccessInterface {

  /**
   * Connection definition.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;

  /**
   * EntityTypeManager definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected  $entityTypeManager;

  /**
   * RequestStack definition.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * Constructor.
   */
  public function __construct(Connection $connection, EntityTypeManagerInterface $entity_type_manager, RequestStack $request_stack) {
    $this->connection = $connection;
    $this->entityTypeManager = $entity_type_manager;
    $this->requestStack = $request_stack;
  }

  /**
   * Custom access function for revision diff links.
   *
   * @param int $left_revision
   *   Left revision id.
   * @param int $right_revision
   *   Right revision id.
   * @param \Drupal\Core\Routing\RouteMatch $route_match
   *   Current route match.
   * @param \Drupal\Core\Session\AccountInterface $account
   *   Current account.
   *
   * @return \Drupal\Core\Access\AccessResult|\Drupal\Core\Access\AccessResultAllowed|\Drupal\Core\Access\AccessResultNeutral
   *   AccessResult.
   */
  public function access(int $left_revision, int $right_revision, RouteMatch $route_match, AccountInterface $account) {
    // Revision access.
    $left_access = $this->entityTypeManager->getStorage('node')->loadRevision($left_revision)->access('view', $account);
    $right_access = $this->entityTypeManager->getStorage('node')->loadRevision($right_revision)->access('view', $account);

    // Get token that has not expiry date or hasn't expired.
    $token = $this->requestStack->getCurrentRequest()->query->get('token');
    if ($token) {
      $query = $this->connection->select('preview_diff', 'pd')
        ->fields('pd', ['token'])
        ->condition('left_revision', $left_revision)
        ->condition('right_revision', $right_revision)
        ->condition('token', $token);

      $no_expiry = $query->orConditionGroup()
        ->condition('expiry', 0)
        ->condition('expiry', time(), '>');

      $results = $query->condition($no_expiry)->execute();
      $match = $results->fetch();
    }

    return AccessResult::allowedIf(($account->isAuthenticated() && $left_access && $right_access) || $match);

  }

}
