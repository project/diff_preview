<?php

namespace Drupal\diff_preview\Controller;

use Drupal\diff\Controller\NodeRevisionController;
use Drupal\node\NodeInterface;

/**
 * Extends default revision controller with token appended links + form.
 */
class PreviewDiffController extends NodeRevisionController {

  /**
   * {@inheritdoc}
   */
  public function compareNodeRevisions(NodeInterface $node, $left_revision, $right_revision, $filter) {
    $build = parent::compareNodeRevisions($node, $left_revision, $right_revision, $filter);

    // Add token to all different viewing links.
    $token = $this->requestStack->getCurrentRequest()->query->get('token');
    if ($token) {
      $layout_options =& $build['controls']['diff_layout']['filter']['#links'];
      foreach ($layout_options as &$layout_option) {
        $layout_option['url'] = $layout_option['url']->setRouteParameter('token', $token);
      }

      $filter_options =& $build['controls']['filter']['options']['options']['#links'];
      if ($filter_options) {
        foreach ($filter_options as &$filter_option) {
          $filter_option['url'] = $filter_option['url']->setRouteParameter('token', $token);
        }
      }

      $view_mode_options =& $build['controls']['view_mode']['filter']['#links'];
      if ($view_mode_options) {
        foreach ($view_mode_options as &$view_mode_option) {
          $view_mode_option['url'] = $view_mode_option['url']->setRouteParameter('token', $token);
        }
      }
    }

    // Add generating form.
    $preview_form = $this->formBuilder()->getForm('Drupal\gstt_preview_diff\Form\GetPreviewDiffLinkForm', $left_revision, $right_revision);
    array_splice($build, 2, 0, ['form' => $preview_form]);
    $build['#cache']['contexts'][] = 'url.query_args';
    return $build;
  }

}
