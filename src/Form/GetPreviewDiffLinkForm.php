<?php

namespace Drupal\diff_preview\Form;

use Drupal\Core\Database\Connection;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provide generating preview link form on revision diff page.
 */
class GetPreviewDiffLinkForm extends FormBase {

  /**
   * Connection definition.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;

  /**
   * Class constructor.
   */
  public function __construct(Connection $connection) {
    $this->connection = $connection;
  }

  /**
   * Dependency injection.
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('database')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'diff_preview.preview_link_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $left_revision = NULL, $right_revision = NULL) {

    if (isset($left_revision) && isset($right_revision)) {
      $form_state->set('left_revision', $left_revision);
      $form_state->set('right_revision', $right_revision);
      // Find token.
      $query = $this->connection->select('preview_diff', 'pd')
        ->fields('pd', ['pid', 'expiry', 'token'])
        ->condition('left_revision', $left_revision)
        ->condition('right_revision', $right_revision);

      $results = $query->execute();

      $match = $results->fetch();

      // Populate with viewable URL.
      if ($match->token) {
        $form_state->set('pid', $match->pid);
        $access_link = Url::fromRoute('<current>', ['token' => $match->token]);

        $form['link_field'] = [
          '#type' => 'url',
          '#title' => $this->t('Diff preview link'),
          '#disabled' => TRUE,
          '#required' => TRUE,
          '#default_value' => $access_link->setAbsolute()->toString(),
        ];

      }
    }

    // Token expiry date.
    $form['expiry'] = [
      '#type' => 'date',
      '#title' => $this->t('Expiry date'),
    ];

    if ($match->expiry && $match->expiry != 0) {
      $form['expiry']['#default_value'] = date('Y-m-d', $match->expiry);
    }

    // Only allows users with permissions to regenerate/generate tokens.
    if ($this->currentUser()->hasPermission('generate preview diff links')) {
      $form['actions'] = [
        '#type' => 'actions',
      ];

      $form['actions']['submit'] = [
        '#type' => 'submit',
        '#value' => $this->t('Generate link'),
      ];
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $update_values = [
      'token' => bin2hex(random_bytes(32)),
    ];
    if ($expiry = $form_state->getValue('expiry')) {
      $update_values['expiry'] = strtotime($expiry);
    }

    if ($form_state->get('pid')) {
      // Regenerate token.
      $this->connection->update('preview_diff')
        ->fields($update_values)
        ->condition('left_revision', $form_state->get('left_revision'))
        ->condition('right_revision', $form_state->get('right_revision'))
        ->execute();
    }
    else {
      // Create token.
      $update_values['left_revision'] = $form_state->get('left_revision');
      $update_values['right_revision'] = $form_state->get('right_revision');
      $this->connection->insert('preview_diff')
        ->fields($update_values)
        ->execute();
    }
  }

}
